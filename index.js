function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    if (letter.length === 1){

        // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
        for(let i = 0; i < sentence.length; i++){
            if(letter === sentence[i]) {
                // increment if theres a match
                result += 1
            } 
        }
        // returns the result count
        return result;
    } else {
        // If letter is invalid, return undefined.
        return undefined;
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.

    // Convert text into lowercase
    // The function should disregard text casing before doing anything else.
    text = text.toLowerCase();

    for (var i=0; i<text.length; i++) {
        // If the function finds a repeating letter, 
        if ( text.indexOf(text[i]) !== text.lastIndexOf(text[i]) ) {
            return false; // repeats return false. 
        }
    }
    // Otherwise, return true.
    return true;
}

function purchase(age, price) {
    // The returned value should be a string.
    // result value should always show two decimal places
    
    let discountedPrice = price * 0.2;

    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    } else if ( age >= 13 && age <= 21 || age >= 65) {
        return parseFloat(price - discountedPrice).toFixed(2);
    // Return the rounded off price for people aged 22 to 64.
    } else if (age >= 22 && age <= 64){
        return parseFloat(price).toFixed(2);
    }
}

function findHotCategories(items) {
    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

    let hotCategories = [];

    // Find categories that has no more stocks.
    for (let i = 0; i < items.length; i++) {
        if(items[i].stocks === 0) {
            hotCategories.push(items[i].category)
        }
    }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // The hot categories must be unique; no repeating categories.
    hotCategories = [...new Set(hotCategories)];
    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    let flyingVoters = [];

    // Find voters who voted for both candidate A and candidate B.
    for (let i = 0; i < candidateA.length; i++ ){
        if(candidateB.indexOf(candidateA[i]) !== -1 ){
            flyingVoters.push(candidateA[i])
        }
    }
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    return flyingVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};